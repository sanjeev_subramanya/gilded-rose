package com.gildedrose


class AgedBrieStrategy : Degradable {
    companion object {
        const val NAME: String = "Aged Brie"
    }
    override fun degrade(item: Item) {
        if (item.quality < MAX_QUALITY) {
            item.quality = item.quality + 1
        }
        item.sellIn--
    }
}