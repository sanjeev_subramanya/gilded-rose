package com.gildedrose


class BackStageItemStrategy : Degradable {
    companion object {
        const val NAME: String = "Backstage passes to a TAFKAL80ETC concert"
    }
    override fun degrade(item: Item) {
        val incrementValue = when {
            item.sellIn < 6 -> {
                3
            }
            item.sellIn < 11 -> {
                2
            }
            else -> {
                1
            }
        }
        val value = item.quality + incrementValue
        if (value < MAX_QUALITY) {
            item.quality = item.quality + incrementValue
        }

        item.sellIn--
    }


}