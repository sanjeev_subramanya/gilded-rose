package com.gildedrose

class ConjuredStrategy : Degradable {
    companion object {
        const val NAME = "Conjured Mana Cake"
    }
    override fun degrade(item: Item) {
        val depreciation = if (item.sellIn <= 0) {
            item.quality - 4
        } else {
            item.quality - 2
        }

        if (depreciation <= MIN_QUALITY) {
            item.quality = 0
        } else {
            item.quality = depreciation
        }
        item.sellIn--
    }
}