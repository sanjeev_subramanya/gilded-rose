package com.gildedrose

import com.gildedrose.Item

interface Degradable {
    val MIN_QUALITY: Int
        get() = 0
    val MAX_QUALITY: Int
        get() = 50

    fun degrade(item: Item)
}