package com.gildedrose

class DegradationStrategyFactory {
    companion object {
        fun getDegradationStrategyForItem(item: Item): Degradable {//Can be further improved by a flyweight like pattern
            return when (item.name) {
                AgedBrieStrategy.NAME -> {
                    AgedBrieStrategy()
                }
                BackStageItemStrategy.NAME -> {
                    BackStageItemStrategy()
                }
                SulfurasStrategy.NAME -> {
                    SulfurasStrategy()
                }
                ConjuredStrategy.NAME -> {
                    ConjuredStrategy()
                }
                else -> {
                    GenericStrategy()
                }
            }
        }
    }
}