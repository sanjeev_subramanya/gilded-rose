package com.gildedrose

import com.gildedrose.Item

class GenericStrategy : Degradable {
    override fun degrade(item: Item) {
        val depreciation = if (item.sellIn <= 0) {
            item.quality - 2
        } else {
            item.quality - 1
        }
        if (depreciation <= MIN_QUALITY) {
            item.quality = 0
        } else {
            item.quality = depreciation
        }
        item.sellIn--
    }
}