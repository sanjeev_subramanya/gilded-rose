package com.gildedrose

class GildedRose(var items: Array<Item>) {

   /* fun updateQuality() {
     /**Begin implmentation **/
          for (i in items.indices) {
              if (items[i].name == "Conjured") {
                  val depreciation = if (items[i].sellIn <= 0) {
                      items[i].quality - 4
                  } else {
                      items[i].quality - 2
                  }
                  if (depreciation <= 0) {
                      items[i].quality = 0
                  } else {
                      items[i].quality = depreciation
                  }
                  items[i].sellIn--
                  continue
              }
               /**End implmentation **/

              if (items[i].name != "Aged Brie" && items[i].name != "Backstage passes to a TAFKAL80ETC concert") {
                  if (items[i].quality > 0) {
                      if (items[i].name != "Sulfuras, Hand of Ragnaros") {
                          items[i].quality = items[i].quality - 1
                      }
                  }
              } else {
                  if (items[i].quality < 50) {
                      items[i].quality = items[i].quality + 1

                      if (items[i].name == "Backstage passes to a TAFKAL80ETC concert") {
                          if (items[i].sellIn < 11) {
                              if (items[i].quality < 50) {
                                  items[i].quality = items[i].quality + 1
                              }
                          }

                          if (items[i].sellIn < 6) {
                              if (items[i].quality < 50) {
                                  items[i].quality = items[i].quality + 1
                              }
                          }
                      }
                  }
              }

              if (items[i].name != "Sulfuras, Hand of Ragnaros") {
                  items[i].sellIn = items[i].sellIn - 1
              }

              if (items[i].sellIn < 0) {
                  if (items[i].name != "Aged Brie") {
                      if (items[i].name != "Backstage passes to a TAFKAL80ETC concert") {
                          if (items[i].quality > 0) {
                              if (items[i].name != "Sulfuras, Hand of Ragnaros") {
                                  items[i].quality = items[i].quality - 1
                              }
                          }
                      } else {
                          items[i].quality = items[i].quality - items[i].quality
                      }
                  } else {
                      if (items[i].quality < 50) {
                          items[i].quality = items[i].quality + 1
                      }
                  }
              }
          }
      } */

    fun updateQuality() {
        items.forEach {
            DegradationStrategyFactory.getDegradationStrategyForItem(it).degrade(it)
        }
    }


}











