package com.gildedrose


class SulfurasStrategy : Degradable {
    companion object {
        const val NAME = "Sulfuras, Hand of Ragnaros"
    }

    override fun degrade(item: Item) {}
}