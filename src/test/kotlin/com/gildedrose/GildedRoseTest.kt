package com.gildedrose

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class GildedRoseTest {

    /**
     * Backstage passes
     */
    @Test
    fun `case{ Backstage passes 11 days to sell} - Once the sell by date has passed, Quality degrades twice as fast`() {
        val items = arrayOf<Item>(Item("Backstage passes to a TAFKAL80ETC concert", 11, 10))
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(11, app.items[0].quality)
        assertEquals(10, app.items[0].sellIn)
    }

    @Test
    fun `case{ Backstage passes 10 days to sell} - Once the sell by date has passed, Quality degrades twice as fast`() {
        val items = arrayOf<Item>(Item("Backstage passes to a TAFKAL80ETC concert", 10, 10))
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(12, app.items[0].quality)
        assertEquals(9, app.items[0].sellIn)
    }

    @Test
    fun `case{ Backstage passes 5 days to sell } - Once the sell by date has passed, Quality degrades twice as fast`() {
        val items = arrayOf<Item>(Item("Backstage passes to a TAFKAL80ETC concert", 5, 10))
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(13, app.items[0].quality)
        assertEquals(4, app.items[0].sellIn)
    }

    @Test
    fun `The Quality of an item is never negative`() {
        val items = arrayOf<Item>(Item("foo", 2, 0))
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(0, app.items[0].quality)
        assertEquals(1, app.items[0].sellIn)
    }

    /**
     * Aged Brie
     */
    @Test
    fun `case{ Aged Brie } actually increases in Quality the older it gets`() {
        val items = arrayOf<Item>(Item("Aged Brie", 2, 10))
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(11, app.items[0].quality)
        assertEquals(1, app.items[0].sellIn)
    }


    /**
     * Sulfuras
     */

    @Test
    fun `"Sulfuras", being a legendary item, never has to be sold or decreases in Quality`() {
        val items = arrayOf<Item>(Item("Sulfuras, Hand of Ragnaros", 10, 10))
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(10, app.items[0].quality)
        assertEquals(10, app.items[0].sellIn)
    }

    /**
     * Conjured items
     */

    @Test
    fun `"Conjured" items degrade in Quality twice as fast as normal items`() {
        val items = arrayOf<Item>(Item("Conjured", 10, 10))
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(8, app.items[0].quality)
        assertEquals(9, app.items[0].sellIn)
    }

    @Test
    fun `"Conjured" items degrade in Quality twice as fast as normal items after passing sellin date`() {
        val items = arrayOf<Item>(Item("Conjured", 0, 10))
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(6, app.items[0].quality)
        assertEquals(-1, app.items[0].sellIn)
    }

    /**
     * All other items test cases
     */

    @Test
    fun `"All other" items degrade in Quality twice as fast as normal items`() {
        val items = arrayOf<Item>(Item("foo", 1, 10))
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(9, app.items[0].quality)
        assertEquals(0, app.items[0].sellIn)
    }


    @Test
    fun `Once the sell by date has passed, Quality degrades twice as fast`() {
        val items = arrayOf<Item>(Item("foo", 0, 10))
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(8, app.items[0].quality)
        assertEquals(-1, app.items[0].sellIn)
    }

    @Test
    fun `The Quality of an item is never more than 50`() {
        val items = arrayOf<Item>(
            Item("Aged Brie", 10, 50),
            Item("Backstage passes to a TAFKAL80ETC concert", 10, 50)
        )
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(50, app.items[0].quality)
        assertEquals(9, app.items[0].sellIn)
    }


    @Test
    fun `Quality drops to 0 after the concert`() {

    }

}


/**
1. Add a new feature
2. SellIn - numbers of days to to sell the item
3. Quality - how valuable the item is
4. after every day both the values are lowered

Requirements
- Once the sell by date has passed, Quality degrades twice as fast
- The Quality of an item is never negative
- "Aged Brie" actually increases in Quality the older it gets
- The Quality of an item is never more than 50
- "Sulfuras", being a legendary item, never has to be sold or decreases in Quality
- "Backstage passes", like aged brie, increases in Quality as its SellIn value approaches;
Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but
Quality drops to 0 after the concert
 **/
